﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;

namespace AudioAnalyzer
{
    class AudioAnalyzer
    {
        bool _valid = false;
        object _mutex = new object();

        float SMOOTH_LEVEL = 50;

        int _size = 0;
        float _totatMagnitude = 0;
        float _bass = 0;
        float _previousBass = 0;
        float[] _real;
        float[] _waveform1;
        float[] _waveform2;
        float[] _imaginary;
        float[] _magnitude;
        float[] _smoothMagnitude;
        float[] _maximums;
        float _level;

        public AudioAnalyzer(int sizeOfVectors)
        {
            _size = sizeOfVectors;
            _totatMagnitude = 0f;
            _real = new float[sizeOfVectors];
            _waveform1 = new float[sizeOfVectors];
            _waveform2 = new float[sizeOfVectors];
            _imaginary = new float[sizeOfVectors];
            _magnitude = new float[sizeOfVectors];
            _smoothMagnitude = new float[sizeOfVectors];
            _maximums = new float[sizeOfVectors];
        }

        public unsafe void NextFrame(float* dataInFloat)
        {
            // synchronize write operation
            lock (_mutex)
            {
                // copy data to C# array from vector
                // and fill imaginary[] with 0
                for (int i = 1; i < _size; i += 1)
                {
                    float data = dataInFloat[i] * i / 1000 ;
                    _real[i] = data;
                    _waveform1[i] = data;
                    _waveform2[i] = data;
                    _imaginary[i] = 0f;
                }
                // invalidate everything
                _valid = false;
            }
        }

        public void Calculate()
        {
            // sync read operation
            lock (_mutex)
            {
                // get FFT from current frame
                Fourier.Forward(_real, _imaginary);

                for (int i = 1; i < _size / 2; i += 1)
                {
                    _magnitude[i] = (float)Math.Sqrt(_real[i] * _real[i] + _imaginary[i] * _imaginary[i]);
                }
            }

            BassCalculation();

            // smooth magnitude
            SmoothMagnitudeCalculation();

            MaximumsCalculation();

            LevelCalculation();

            // mark this frame as valid
            _valid = true;
        }

        private void MaximumsCalculation()
        {
            for (int i = 1; i < _size / 2; i += 1)
            {
                _maximums[i] = Math.Max(_maximums[i], _magnitude[i]);
                if (_maximums[i] > 0f)
                    _maximums[i] -= 0.01f;
            }
        }

        private void BassCalculation()
        {
            // bass level calculation
            var newbass = _magnitude[0] * 3 + _magnitude[1] * 2 + _magnitude[2];
            newbass /= 6;
            var xbass = Math.Max(_previousBass, newbass);
            _previousBass = newbass;
            _bass = (_bass + xbass) / 2;
        }

        private void LevelCalculation()
        {
            _level = 0;
            // total level calculation
            for (int i = 1; i < _size / 2; i += 1)
            {
                _level += _smoothMagnitude[i];
            }
            //_level /= (_size / 2);
        }

        private void SmoothMagnitudeCalculation()
        {
            var smooth1 = SMOOTH_LEVEL - 1f;
            for (int i = 1; i < _size / 2; i += 1)
            {
                _smoothMagnitude[i] = (_smoothMagnitude[i] * smooth1 + _magnitude[i]) / SMOOTH_LEVEL;
            }
        }

        public void Validate()
        {
            if (!_valid)
                Calculate();
        }

        public float TotatMagnitude
        {
            get
            {
                Validate();
                return _totatMagnitude;
            }
        }
        public float BassLevel
        {
            get
            {
                Validate();
                return _bass;
            }
        }
        public float PreviousBass
        {
            get
            {
                Validate();
                return _previousBass;
            }
        }

        public float[] Real
        {
            get
            {
                Validate();
                return _real;
            }
        }

        public float[] Waveform1
        {
            get
            {
                Validate();
                return _waveform1;
            }
        }

        public float[] Waveform2
        {
            get
            {
                Validate();
                return _waveform2;
            }
        }

        public float[] Imaginary
        {
            get
            {
                Validate();
                return _imaginary;
            }
        }

        public float[] Magnitude
        {
            get
            {
                Validate();
                return _magnitude;
            }
        }

        public float[] SmoothMagnitude
        {
            get
            {
                Validate();
                return _smoothMagnitude;
            }
        }

        public float[] Maximums
        {
            get
            {
                Validate();
                return _maximums;
            }
        }

        public float Level
        {
            get
            {
                Validate();
                return _level;
            }
        }
    }
}
