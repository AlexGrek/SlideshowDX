﻿using MathNet.Numerics.IntegralTransforms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace AudioAnalyzer
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            DataContext = this;
        }

        AudioPlayer _player;
        AudioAnalyzer _analyst;
        bool IsPaused;
        float[] graph1 = new float[1024];
        float[] graph2 = new float[1024];

        public bool XGraph;
        public bool XMagnitude;
        public bool XLevel;
        public bool XSmoothMagnitude;


        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            filePicker.FileTypeFilter.Add(".mp3");
            filePicker.FileTypeFilter.Add(".wav");
            filePicker.FileTypeFilter.Add(".wma");
            filePicker.FileTypeFilter.Add(".m4a");
            filePicker.ViewMode = PickerViewMode.Thumbnail;
            StorageFile file = await filePicker.PickSingleFileAsync();

            // File can be null if cancel is hit in the file picker
            if (file == null)
            {
                return;
            }

            await LoadAudio(file);
        }

        private async Task LoadAudio(StorageFile song)
        {
            if (_analyst == null)
            {
                _analyst = new AudioAnalyzer(960);
                Debug.WriteLine("Anal loaded");           
             }
            if (_player == null)
            {
                _player = new AudioPlayer(song, _analyst);
                Debug.WriteLine("Player loaded");
                await _player.InitializeAsync();
                _player.Play();
                Debug.WriteLine("Player started");
                RunLoop(17);
            }
            
        }

        private void RunLoop(int delay)
        {
            TimeSpan period = TimeSpan.FromMilliseconds(delay);
            IsPaused = false;
            ThreadPoolTimer PeriodicTimer = ThreadPoolTimer.CreatePeriodicTimer((source) =>
            {
                if (!IsPaused)
                {
                    _analyst.Validate();
                    myLittleCanvas.Invalidate();
                }
            }, period);
        }

        private void myLittleCanvas_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {

        }

        /// <summary>
        /// Draw graphs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void canvasControl_Draw(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasDrawEventArgs args)
        {
            var x = 25;

            if (_analyst == null)
                return;

            //for (int i = 0; i < _analyst.Imaginary.Length; i++)
            //{
            //    float grabbed = _analyst.Imaginary[i];
            //    if (grabbed <= 0)
            //        args.DrawingSession.DrawRectangle(new Rect(300, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Chocolate);
            //    else
            //        args.DrawingSession.DrawRectangle(new Rect(300 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.AliceBlue);
            //}
            //for (int i = 0; i < _analyst.Real.Length; i++)
            //{
            //    float grabbed = _analyst.Real[i];
            //    if (grabbed <= 0)
            //        args.DrawingSession.DrawRectangle(new Rect(600, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Snow);
            //    else
            //        args.DrawingSession.DrawRectangle(new Rect(600 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.LightPink);
            //}
            for (int i = 0; i < _analyst.Magnitude.Length/x; i++)
            {
                float grabbed = _analyst.Magnitude[i] * 0.5f;
                if (grabbed <= 0)
                    args.DrawingSession.FillRectangle(new Rect(100 + (i * 4), 400, 3, (-grabbed) * 100), Colors.Aqua);
                else
                    args.DrawingSession.FillRectangle(new Rect(100 + (i * 4), 400 - (grabbed) * 100, 3, (grabbed) * 100), Colors.Khaki);
            }
            for (int i = 0; i < _analyst.SmoothMagnitude.Length/x; i++)
            {
                float grabbed = _analyst.SmoothMagnitude[i] * 0.5f;
                if (grabbed <= 0)
                    args.DrawingSession.DrawRectangle(new Rect(100 + (i * 4), 400, 4, (-grabbed) * 100), Colors.HotPink);
                else
                    args.DrawingSession.DrawRectangle(new Rect(100 + (i * 4), 400 - (grabbed) * 100, 4, (grabbed) * 100), Colors.HotPink);
            }
            
            for (int i = 0; i < _analyst.Maximums.Length/x; i++)
            {
                float grabbed = _analyst.Maximums[i] * 0.5f;
                if (grabbed <= 0)
                    args.DrawingSession.DrawRectangle(new Rect(100 + (i * 4), 400, 4, (-grabbed) * 100), Colors.BlueViolet);
                else
                    args.DrawingSession.DrawRectangle(new Rect(100 + (i * 4), 400 - (grabbed) * 100, 4, 2), Colors.Red);
            }

            if (XGraph)
            {
                args.DrawingSession.DrawText(graph2[0].ToString(), 99, 99, Colors.PapayaWhip);
                float voffset = 40f;
                for (int i = graph1.Length - 2; i >= 0; i -= 1)
                {
                    args.DrawingSession.DrawLine(graph1[i] * 10, i + voffset, graph1[i + 1] * 10, i + 1 + voffset, Colors.Lavender);
                    graph1[i + 1] = graph1[i]; //propagate
                }
                for (int i = graph2.Length - 2; i >= 0; i -= 1)
                {
                    args.DrawingSession.DrawLine(graph2[i] * 100, i + voffset, graph2[i + 1] * 100, i + 1 + voffset, Colors.LemonChiffon);
                    graph2[i + 1] = graph2[i]; //propagate
                }
                bool pause = true;
                for (int i = 0; i < length; i++)
                {
                    if (graph2[i] > gate)
                    {
                        pause = false;
                        break;
                    }
                }
                if (pause)
                {
                    args.DrawingSession.DrawText("PAUSE", 99, 299, Colors.PapayaWhip);
                } else
                {
                    args.DrawingSession.DrawText("WORD", 99, 299, Colors.DeepSkyBlue);
                }

            }

            if (XLevel)
            {
                // level
                float grabbed = _analyst.Level;
                args.DrawingSession.FillRectangle(new Rect(10, 10, grabbed * 10, 10), Colors.Red);
                graph1[0] = grabbed;
            }
            
            if (XLevel)
            {
                // basslevel
                float grabbed = _analyst.BassLevel;
                args.DrawingSession.FillRectangle(new Rect(10, 25, grabbed * 100, 10), Colors.Blue);
                graph2[0] = grabbed;
            }

            //// WAVEFORMS
            //for (int i = 0; i < _analyst.Waveform1.Length; i++)
            //{
            //    float grabbed = _analyst.Waveform1[i];
            //    if (grabbed <= 0)
            //        args.DrawingSession.DrawRectangle(new Rect(1200, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Honeydew);
            //    else
            //        args.DrawingSession.DrawRectangle(new Rect(1200 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.Ivory);
            //}

            //for (int i = 0; i < _analyst.Waveform2.Length; i++)
            //{
            //    float grabbed = _analyst.Waveform2[i];
            //    if (grabbed <= 0)
            //        args.DrawingSession.DrawRectangle(new Rect(1500, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Honeydew);
            //    else
            //        args.DrawingSession.DrawRectangle(new Rect(1500 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.Ivory);
            //}
        }

        private void LevelCB_Click(object sender, RoutedEventArgs e)
        {
            XLevel = LevelCB.IsChecked ?? false;
        }

        private void GraphCB_Click(object sender, RoutedEventArgs e)
        {
            XGraph = GraphCB.IsChecked ?? false;
        }

        private void ButtonFFT_Click(object sender, RoutedEventArgs e)
        {
            Fourier.Forward(graph1, graph2);
        }

        float gate = 0.50f;
        int length = 30;

        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            gate = (float)e.NewValue / 100f;
        }

        private void Slider_ValueChanged2(object sender, RangeBaseValueChangedEventArgs e)
        {
            length = (int)e.NewValue;
        }
    }
}
