﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Media;
using Windows.Media.Audio;
using Windows.Storage;

namespace AudioAnalyzer
{
    class AudioPlayer
    {
        StorageFile _file;

        AudioGraph _audioGraph;
        AudioFrameOutputNode _frameOutputNode;

        private AudioFileInputNode _fileInput;
        private AudioDeviceInputNode _deviceInput;
        private AudioDeviceOutputNode _deviceOutput;

        AudioAnalyzer _analyst;

        [ComImport]
        [Guid("5B0D3235-4DBA-4D44-865E-8F1D0E4FD04D")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        unsafe interface IMemoryBufferByteAccess
        {
            void GetBuffer(out byte* buffer, out uint capacity);
        }

        private void CreateFrameOutputNode()
        {
            _frameOutputNode = _audioGraph.CreateFrameOutputNode();
            _audioGraph.QuantumStarted += AudioGraph_QuantumStarted;
        }

        private void AudioGraph_QuantumStarted(AudioGraph sender, object args)
        {
            AudioFrame frame = _frameOutputNode.GetFrame();
            ProcessFrameOutput(frame);
        }

        unsafe private void ProcessFrameOutput(AudioFrame frame)
        {
            using (AudioBuffer buffer = frame.LockBuffer(AudioBufferAccessMode.Write))
            using (IMemoryBufferReference reference = buffer.CreateReference())
            {
                byte* dataInBytes;
                uint capacityInBytes;
                float* dataInFloat;

                // Get the buffer from the AudioFrame
                ((IMemoryBufferByteAccess)reference).GetBuffer(out dataInBytes, out capacityInBytes);

                dataInFloat = (float*)dataInBytes;

                // pass to analyst and forget
                _analyst.NextFrame(dataInFloat);
            }
        }

        private async Task InitAudioGraphAsync()
        {

            AudioGraphSettings settings = new AudioGraphSettings(Windows.Media.Render.AudioRenderCategory.GameMedia);
            settings.DesiredSamplesPerQuantum = 128;
            settings.QuantumSizeSelectionMode = QuantumSizeSelectionMode.ClosestToDesired;

            CreateAudioGraphResult result = await AudioGraph.CreateAsync(settings);
            if (result.Status != AudioGraphCreationStatus.Success)
            {
                Debug.WriteLine("AudioGraph creation error: " + result.Status.ToString());
            }

            _audioGraph = result.Graph;

            Debug.WriteLine("AudioGraph sample rate: " + _audioGraph.SamplesPerQuantum);

            // INIT DEVICE OUTPUT

            CreateAudioDeviceOutputNodeResult deviceOutputNodeResult = await _audioGraph.CreateDeviceOutputNodeAsync();

            if (deviceOutputNodeResult.Status != AudioDeviceNodeCreationStatus.Success)
            {
                // Cannot create device output node
                Debug.WriteLine(String.Format("Device Output unavailable because {0}", deviceOutputNodeResult.Status.ToString()));
                return;
            }

            _deviceOutput = deviceOutputNodeResult.DeviceOutputNode;

            // INIT FRAME OUTPUT
            CreateFrameOutputNode();
        }

        // ----------- USER DEFINED SHIT -------------------

        public AudioPlayer(StorageFile audio, AudioAnalyzer analst)
        {
            _file = audio;
            _analyst = analst;
        }

        public async Task InitializeAsync()
        {
            await InitAudioGraphAsync();
            CreateAudioFileInputNodeResult fileInputResult = await _audioGraph.CreateFileInputNodeAsync(_file);
            if (AudioFileNodeCreationStatus.Success != fileInputResult.Status)
            {
                // Cannot read input file
                Debug.WriteLine(String.Format("Cannot read input file because {0}", fileInputResult.Status.ToString()));
                return;
            }

            _fileInput = fileInputResult.FileInputNode;

            //// device input
            //CreateAudioDeviceInputNodeResult result = await _audioGraph.CreateDeviceInputNodeAsync(Windows.Media.Capture.MediaCategory.Media);

            //if (result.Status != AudioDeviceNodeCreationStatus.Success)
            //{
            //    // Cannot create device output node
            //    Debug.WriteLine(String.Format("Cannot read input device because {0}", result.Status.ToString()));
            //    return;
            //}

            //_deviceInput = result.DeviceInputNode;

            if (_fileInput.Duration <= TimeSpan.FromSeconds(3))
            {
                // Imported file is too short
                Debug.WriteLine("Please pick an audio file which is longer than 3 seconds");
                _fileInput.Dispose();
                _fileInput = null;
                return;
            }

            //_deviceInput.AddOutgoingConnection(_deviceOutput);
            //_deviceInput.AddOutgoingConnection(_frameOutputNode);
            _fileInput.AddOutgoingConnection(_frameOutputNode);
            _fileInput.AddOutgoingConnection(_deviceOutput);

            // Trim the file: set the start time to 3 seconds from the beginning
            // fileInput.EndTime can be used to trim from the end of file
            _fileInput.StartTime = TimeSpan.FromSeconds(0);
        }

        public void Play()
        {
            _audioGraph.Start();
        }


    }
}
