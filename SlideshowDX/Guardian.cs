﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Threading;

namespace SlideshowDX
{
    class Guardian
    {
        private StorageFile[] _files;
        private Dictionary<string, StorageFile> _blurrs;
        private Pipeline _pipeline;

        public int Required = 0;
        public int Loaded = 0;

        public bool IsPaused = true;

        public Guardian(StorageFile[] files, Dictionary<string, StorageFile> blurrs, Pipeline pipeline)
        {
            _files = files;
            _pipeline = pipeline;
            _blurrs = blurrs;
        }

        public void RunForever(CanvasAnimatedControl sender)
        {
            TimeSpan period = TimeSpan.FromMilliseconds(100);
            IsPaused = false;
            ThreadPoolTimer PeriodicTimer = ThreadPoolTimer.CreatePeriodicTimer(async (source) =>
            {
                if (!IsPaused && Required > Loaded)
                {
                    // start over again
                    if (Required >= _files.Length)
                        Required = 0;
                    Loaded = Required;

                    // blurred BG support
                    CanvasBitmap bg = null;

                    // check if file has it's blurred BG pair
                    var name = _files[Required].Name;
                    if (_blurrs.ContainsKey(name))
                    {
                        bg = await LoadBitmapAsync(_blurrs[name], sender);
                        Debug.WriteLine($"Loaded background for {name}");
                    }

                    var bmp = await LoadBitmapAsync(_files[Required], sender);
                    _pipeline.SetNextBitmap(bmp, bg);
                    Debug.WriteLine($"Loaded next {Loaded}: {_files[Loaded].DisplayName}");
                }
            }, period);
        }

        public async Task<CanvasBitmap> LoadBitmapAsync(StorageFile file, CanvasAnimatedControl sender)
        {
            using (IRandomAccessStream fileStream = await file.OpenReadAsync())
            {
                var bmp = await CanvasBitmap.LoadAsync(sender.Device, fileStream);
                return bmp;
            }
        }
    }
}
