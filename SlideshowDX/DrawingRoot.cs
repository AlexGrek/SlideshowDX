﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;

namespace SlideshowDX
{
    interface DrawingRoot
    {
        void Render(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args, AudioAnalyzer anal, float time);
    }
}
