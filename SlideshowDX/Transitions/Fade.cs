﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas.Effects;
using Windows.UI;

namespace SlideshowDX.Transitions
{
    class Fade : ITransition
    {
        ArithmeticCompositeEffect _fx;
        CanvasRenderTarget _rt;
        CanvasDrawingSession _dt;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _fx = new ArithmeticCompositeEffect()
            {
                MultiplyAmount = 0,
                Offset = 0
            };
            _dt = _rt.CreateDrawingSession();
        }

        public CanvasRenderTarget Render(CanvasRenderTarget prev, CanvasRenderTarget next, float time)
        {

            _fx.Source1Amount = 1 - time;
            _fx.Source2Amount = time;

            _fx.Source1 = prev;
            _fx.Source2 = next;

            var dt = _dt;
            dt.Clear(Windows.UI.Colors.AliceBlue);
            dt.DrawImage(_fx);
            dt.Flush();

            return _rt;
        }
    }
}
