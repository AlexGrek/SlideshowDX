﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Transitions
{
    interface ITransition
    {
        CanvasRenderTarget Render(CanvasRenderTarget prev, CanvasRenderTarget next, float time);
        void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer);
    }
}
