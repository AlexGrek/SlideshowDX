﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Transitions
{
    public class TransitionSettings
    {
        public bool FadeShiftEnabled;
        public bool FadeEnabled;
        public bool FlipEnabled;
    }
}
