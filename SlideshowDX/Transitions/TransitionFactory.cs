﻿using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Transitions
{
    class TransitionFactory
    {
        Random _rand = new Random();
        TransitionSettings _params;
        bool _loaded = false;
        List<Func<ITransition>> _availableTransitions;

        public TransitionFactory(TransitionSettings sets)
        {
            _params = sets;
        }

        public FadeShift UseFadeShift()
        {
            int ND = 4; // total directions
            var chosen = _rand.Next(0, ND);
            FadeShift transition;
            switch (chosen)
            {
                case 0:
                    transition = new FadeShift();
                    break;
                case 1:
                    transition = new FadeShift(-1);
                    break;
                case 2:
                    transition = new FadeShift(-1, true);
                    break;
                case 3:
                    transition = new FadeShift(1, true);
                    break;
                default:
                    throw new Exception("What the hell, we have no such FadeShift direction man");
            }
            return transition;
        }

        public Fade UseFade() => new Fade();

        public Flip UseFlip() => new Flip();

        public ITransition CreateAny(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            int N = 3; // total transitions
            var chosen = _rand.Next(0, N);
            ITransition transition;
            //chosen = 1;
            switch (chosen)
            {
                case 0:
                    transition = new Fade();
                    break;
                case 1:
                    transition = new Flip();
                    break;
                case 2:
                    transition = UseFadeShift();
                    break;
                default:
                    throw new Exception("What the hell, we have no such transition man");

            }
            transition.Load(canvas, analyzer);
            return transition;
        }

        public ITransition CreateSpecial(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            if (!_loaded)
                throw new MethodAccessException("TransitionFactory is not loaded yet, but CreateSpecial called");
            int N = _availableTransitions.Count; // total transitions
            var chosen = _rand.Next(0, N);
            ITransition transition;
            //chosen = 1;
            transition = _availableTransitions[chosen].Invoke();
            transition.Load(canvas, analyzer);
            return transition;
        }

        public async Task Load()
        {
            _availableTransitions = new List<Func<ITransition>>();
            if (_params.FadeEnabled)
                _availableTransitions.Add(UseFade);
            if (_params.FlipEnabled)
                _availableTransitions.Add(UseFlip);
            if (_params.FadeShiftEnabled)
                _availableTransitions.Add(UseFadeShift);
            _loaded = true;
        }
    }
}
