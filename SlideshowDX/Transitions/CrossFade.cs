﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas.Effects;
using Windows.UI;
using static SlideshowDX.AnimationTools.Easing;
using System.Numerics;
using Windows.Foundation;

namespace SlideshowDX.Transitions
{
    class Flip : ITransition
    {
        CanvasRenderTarget _rt;
        CanvasDrawingSession _dt;
        Transform3DEffect _d3d;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _dt = _rt.CreateDrawingSession();
            _d3d = new Transform3DEffect();
        }

        public CanvasRenderTarget Render(CanvasRenderTarget prev, CanvasRenderTarget next, float time)
        {
            var otime = CubicEaseOut(time);
            var opacity = 1f;

            if (otime < 0.5f)
            {
                _d3d.TransformMatrix = Matrix4x4.CreateRotationY(otime * (float)Math.PI, new Vector3((float)prev.Size.Width / 2f, (float)prev.Size.Height / 2f, 0));
                _d3d.Source = prev;
                opacity = 1 - otime / 2;
            } else
            {
                _d3d.TransformMatrix = Matrix4x4.CreateRotationY((1 - otime) * (float)Math.PI, new Vector3((float)prev.Size.Width / 2f, (float)prev.Size.Height / 2f, 0));
                _d3d.Source = next;
                opacity = otime/2 + 0.5f;
            }

            var srcrect = new Rect()
            {
                Height = prev.Size.Height,
                Width = prev.Size.Width
            };

           

            var dt = _dt;
            dt.Clear(Colors.Black);
            dt.DrawImage(_d3d, opacity: opacity, offset: Vector2.Zero, sourceRectangle: srcrect);
            dt.Flush();

            return _rt;
        }
    }
}
