﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas.Effects;
using Windows.UI;
using Windows.Foundation;
using static SlideshowDX.AnimationTools.Easing;

namespace SlideshowDX.Transitions
{
    class FadeShift : ITransition
    {
        DirectionalBlurEffect _fx;
        CanvasRenderTarget _rt;
        CanvasDrawingSession _dt;
        float _factor = 500;
        float _blurFactor = 50;
        bool _vertical;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _fx = new DirectionalBlurEffect()
            {
                BlurAmount = 0
            };
            if (_vertical)
                _fx.Angle = (float)Math.PI / 2f;
            _dt = _rt.CreateDrawingSession();
        }

        public FadeShift(float multiplier = 1f, bool vertical = false)
        {
            _factor *= multiplier;
            _vertical = vertical;
        }

        public CanvasRenderTarget Render(CanvasRenderTarget prev, CanvasRenderTarget next, float intime)
        {
            var time = CubicEaseIn(intime);
            //var time2 = SineEaseIn(intime);

            _fx.BlurAmount = intime * _blurFactor;

            _fx.Source = prev;
            //_fx.Source2 = next;

            var dt = _dt;

            var rect = new Rect(0, 0, prev.Size.Width, prev.Size.Height);

            dt.Clear(Colors.AliceBlue);
            if (_vertical)
            {
                dt.Transform = System.Numerics.Matrix3x2.CreateTranslation(yPosition: -(1 - time) * _factor, xPosition: 0);
            }
            else
            {
                dt.Transform = System.Numerics.Matrix3x2.CreateTranslation(xPosition: -(1 - time) * _factor, yPosition: 0);
            }
            dt.DrawImage(next);
            if (_vertical)
            {
                dt.Transform = System.Numerics.Matrix3x2.CreateTranslation(yPosition: time * _factor, xPosition: 0);
            }
            else
            {
                dt.Transform = System.Numerics.Matrix3x2.CreateTranslation(xPosition: time * _factor, yPosition: 0);
            }
            dt.DrawImage(_fx, System.Numerics.Vector2.Zero, rect, 1 - time);
            dt.Flush();

            return _rt;
        }
    }
}
