﻿using Microsoft.Toolkit.Uwp.UI.Animations;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SlideshowDX
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectDirectoryPage : Page
    {
        StorageFolder _selectedFolder = null;
        StorageFile _audio = null;
        StorageFile[] _slides;
        Effects.EffectsSettings _esettings = null; // by default
        Transitions.TransitionSettings _tsettings = null; // by default

        const int SHOW_THUMBS_LIMIT = 4;

        #region UI_ANIMATIONS
        bool _lovelyA;
        bool _activeA;
        bool _fastA;
        #endregion

        public SelectDirectoryPage()
        {
            this.InitializeComponent();
        }

        private async void SetBGPic(StorageFile sf)
        {
            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.Xaml.Media.XamlCompositionBrushBase"))
            {
                using (IRandomAccessStream fileStream = await sf.OpenReadAsync())
                {
                    // Set the image source to the selected bitmap 
                    BitmapImage bitmapImage = new BitmapImage();
                    bitmapImage.DecodePixelHeight = 1080; //match the target Image.Width, not shown
                    await bitmapImage.SetSourceAsync(fileStream);
                    BackgroundImage.Source = bitmapImage;
                    AcrylicBrush myBrush = new AcrylicBrush
                    {
                        BackgroundSource = AcrylicBackgroundSource.Backdrop,
                        TintColor = Color.FromArgb(0, 0, 0, 0),
                        FallbackColor = Color.FromArgb(0, 0, 0, 0),
                        TintOpacity = 0.6
                    };

                    PicsGrid.Background = myBrush;
                }
            }
            else
            {
                using (IRandomAccessStream fileStream = await sf.OpenReadAsync())
                {
                    // Set the image source to the selected bitmap 
                    BitmapImage bitmapImage = new BitmapImage();
                    bitmapImage.DecodePixelHeight = 400; //match the target Image.Width, not shown
                    await bitmapImage.SetSourceAsync(fileStream);
                    BackgroundImage.Source = bitmapImage;
                }
            }  
        }

        private async Task ShowMePics(StorageFolder folder)
        {
            bool filter(StorageFile x)
            {
                return x.FileType == ".jpg" || x.FileType == ".jpeg" || x.FileType == ".JPG" || x.FileType == ".JPEG";
            }

            var files = await folder.GetFilesAsync();
            var pics = files.Where(x => filter(x)).ToList();
            _slides = pics.ToArray();
            if (_slides.Length == 0)
            {
                var dialog = new MessageDialog("Oops.. cannot find any images here.\nPlease choose another directory.");
                await dialog.ShowAsync();
                return;
            }
            var rdm = new Random();
            var chosen = pics[rdm.Next(pics.Count())];
            SetBGPic(chosen);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var folderPicker = new Windows.Storage.Pickers.FolderPicker
            {
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.Desktop
            };
            folderPicker.FileTypeFilter.Add("*");

            StorageFolder folder = await folderPicker.PickSingleFolderAsync();
            if (folder != null)
            {
                // Application now has read/write access to all contents in the picked folder
                // (including other sub-folder contents)
                Windows.Storage.AccessCache.StorageApplicationPermissions.
                    FutureAccessList.AddOrReplace("PickedFolderToken", folder);
                Debug.WriteLine("Picked folder: " + folder.Name);
                _selectedFolder = folder;
                try
                {
                    await ShowMePics(folder);
                } catch (Exception ex)
                {
                    var dialog = new MessageDialog(ex.ToString());
                    await dialog.ShowAsync();
                }
                ChooseAudioButton.IsEnabled = true;
            }
            else
            {
                Debug.WriteLine("Operation cancelled.");
            }
        }

        private async void SelectAudio(object sender, RoutedEventArgs e)
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker
            {
                ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail,
                SuggestedStartLocation =
                    Windows.Storage.Pickers.PickerLocationId.PicturesLibrary
            };
            picker.FileTypeFilter.Add(".mp3");
            picker.FileTypeFilter.Add(".flac");

            StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                // Application now has read/write access to the picked file
                _audio = file;

                if (randomize.IsChecked.HasValue && randomize.IsChecked.Value == true)
                {
                    Shuffle(_slides);
                }

                UseCustomSettings();

                var parameters = new SlideshowParams
                {
                    Audio = _audio,
                    Slides = _slides,
                    AvgDelay = AvgDelaySlider.Value,
                    MinDelay = MinDelaySlider.Value,
                    TransitionTime = TransitionTimeSlider.Value,
                    Adaptive = Adaptive.IsChecked ?? false,
                    EffectSet = _esettings,
                    TransitionSet = _tsettings
                };
                if (!AnimationExtensions.BlurEffect.IsSupported)
                {
                    Debug.WriteLine("Blur effect is not supported!");
                }
                await TotalGrid.Blur(value: 30f, duration: 1000).Fade(value: 0f, duration: 1000).StartAsync();
                Frame.Navigate(typeof(SlideshowPage), parameters);
            }
            else
            {
                Debug.WriteLine("Audio not selected");
            }
        }

        private void UseCustomSettings()
        {
            if (_esettings != null && _tsettings != null)
                return;

            // Custom
            _esettings = new Effects.EffectsSettings()
            {
                ZoomBassEnabled = ZoomBassCheckbox.IsChecked ?? false,
                VignetteEnabled = true,
                BlurBGEnabled = true,
                SmoothMoveEnabled = SmoothMoveCheckbox.IsChecked ?? false
            };
            _tsettings = new Transitions.TransitionSettings()
            {
                FadeEnabled = true,
                FlipEnabled = false,
                FadeShiftEnabled = false
            };
        }

        private static Random rnd = new Random();

        /// <summary>
        /// Randomize arrays
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(T[] list)
        {
            int n = list.Length;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            if (StandardPopup.IsOpen) { StandardPopup.IsOpen = false; }
        }

        private void SettingsPop(object sender, RoutedEventArgs e)
        {
            if (StandardPopup.IsOpen) { StandardPopup.IsOpen = false; } else
            {
                StandardPopup.IsOpen = true;
            }
        }

        private void AvgDelaySlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            BalanceDelaySliders();
        }

        public void BalanceDelaySliders()
        {
            if (AvgDelaySlider != null && MinDelaySlider != null)
            {
                if (AvgDelaySlider.Value < MinDelaySlider.Value)
                {
                    MinDelaySlider.Value = AvgDelaySlider.Value;
                }
            }
        }

        private async void Border_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await FastAnimate();
        }

        private async Task FastAnimate()
        {
            await FastKey.Child.Offset(duration: 200, easingType: EasingType.Circle, offsetX: 40).StartAsync();
            await FastKey.Child.Offset(duration: 300, easingType: EasingType.Quintic, offsetX: -40).StartAsync();
            await FastKey.Child.Offset(offsetX: 0, duration: 300, easingType: EasingType.Back).StartAsync();
        }

        private async void FastKey_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            await FastAnimate();
        }

        private async void Border_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (!_activeA)
            {
                _activeA = true;
                try
                {
                    await ActiveKey.Blur(value: 10, duration: 300).StartAsync();
                    await ActiveKey.Blur(value: 0, duration: 300).StartAsync();
                }
                catch (ArgumentException) {
                    await Task.Delay(1000);
                    await ActiveKey.Blur(value: 0, duration: 300).StartAsync();
                }
                _activeA = false;
            }
        }

        private async void LovelyKey_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            await LovelyKey.Light( duration: 500, distance: 100).StartAsync();
            await LovelyKey.Light(duration: 500, distance: 1000).StartAsync();
        }

        private async void Border_PointerEntered_1(object sender, PointerRoutedEventArgs e)
        {
            await CustomKey.Rotate(duration: 500, value: 180, centerX: 20f, centerY: 20f,
                easingType: EasingType.Circle).StartAsync();
            await CustomKey.Rotate(duration: 700, value: 0, centerX: 20f, centerY: 20f).StartAsync();
        }

        private void PicsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = PicsGrid.SelectedIndex;
            try
            {
                var onj = e.ClickedItem;
                item = PicsGrid.Items.IndexOf(onj);
            }
            catch (Exception) { }
            ChooseMode(item);
        }

        private void ChooseMode(int item)
        {
            ModeText.Text = $"ERROR: {item}";
            if (item == 0)
            {
                // Lovely
                ModeText.Text = "Lovely";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = false,
                    VignetteEnabled = true,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true,
                    HeartsEnabled = true
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = false,
                    FadeShiftEnabled = false
                };
                AvgDelaySlider.Value = 4.0;
                MinDelaySlider.Value = 3.5;
                TransitionTimeSlider.Value = 600;
            }
            if (item == 1)
            {
                // Active
                ModeText.Text = "Active";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = true,
                    VignetteEnabled = false,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = false,
                    FlipEnabled = true,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 3.0;
                MinDelaySlider.Value = 1.0;
                TransitionTimeSlider.Value = 200;
            }
            if (item == 2)
            {
                // Fast
                ModeText.Text = "Fast";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = true,
                    VignetteEnabled = true,
                    VignetteColor = Windows.UI.Colors.Black,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = false
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = true,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 2.0;
                MinDelaySlider.Value = 1.0;
                TransitionTimeSlider.Value = 100;
            }
            if (item == 9)
            {
                // Custom
                ModeText.Text = "Custom";
                // reset all settings
                _tsettings = null;
                _esettings = null;
            }
            if (item == 3)
            {
                // Fantastic
                ModeText.Text = "Fantastic";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = true,
                    VignetteEnabled = true,
                    VignetteColor = Windows.UI.Colors.Purple,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = false
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = true,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 10.0;
                MinDelaySlider.Value = 1.0;
                TransitionTimeSlider.Value = 400;
            }
            if (item == 4)
            {
                // Positive
                ModeText.Text = "Positive :)";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = false,
                    VignetteEnabled = true,
                    VignetteColor = Windows.UI.Colors.Purple,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = false,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 8.0;
                MinDelaySlider.Value = 2.0;
                TransitionTimeSlider.Value = 400;
            }
            if (item == 5)
            {
                // Impressive
                ModeText.Text = "Impressive";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = true,
                    VignetteEnabled = true,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true,
                    HeartsEnabled = true
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = true,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 9.0;
                MinDelaySlider.Value = 1.0;
                TransitionTimeSlider.Value = 300;
            }
            if (item == 6)
            {
                // Dynamic
                ModeText.Text = "Dynamic";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = true,
                    VignetteEnabled = false,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true,
                    HeartsEnabled = false
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = false,
                    FlipEnabled = false,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 4.0;
                MinDelaySlider.Value = 2.0;
                TransitionTimeSlider.Value = 400;
            }
            if (item == 7)
            {
                // Fresh
                ModeText.Text = "Fresh";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = false,
                    VignetteEnabled = false,
                    BlurBGEnabled = true,
                    SmoothMoveEnabled = true,
                    HeartsEnabled = false
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = false,
                    FlipEnabled = false,
                    FadeShiftEnabled = true
                };
                AvgDelaySlider.Value = 4.0;
                MinDelaySlider.Value = 2.0;
                TransitionTimeSlider.Value = 400;
            }
            if (item == 8)
            {
                // Simple
                ModeText.Text = "SIMPLE";
                _esettings = new Effects.EffectsSettings()
                {
                    ZoomBassEnabled = false,
                    VignetteEnabled = false,
                    BlurBGEnabled = false,
                    SmoothMoveEnabled = true,
                    HeartsEnabled = false
                };
                _tsettings = new Transitions.TransitionSettings()
                {
                    FadeEnabled = true,
                    FlipEnabled = false,
                    FadeShiftEnabled = false
                };
                AvgDelaySlider.Value = 5.0;
                MinDelaySlider.Value = 2.0;
                TransitionTimeSlider.Value = 800;
            }
        }
    }
}
