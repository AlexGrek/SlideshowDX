﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Brushes;
using Microsoft.Graphics.Canvas.Text;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System.Numerics;
using Windows.Foundation;
using Windows.UI;
using System.Collections.Generic;
using System;
using Windows.UI.Xaml.Media;

namespace SlideshowDX.Effects
{
    class Hearts : IEffect
    {
        class Heart
        {
            Rect _position;
            ICanvasBrush _brush;
            public float Lifetime = 1f; // like 100% HP
            CanvasTextFormat _format;
            float _speed;
            Size _size = new Size(100.0, 100.0);

            public Heart(ICanvasAnimatedControl canvas, Random rdm, Vector2 center, float minspeed, float maxspeed)
            {
                var wigX = (float)rdm.NextDouble() * center.X * 2;
                var wigY = (float)rdm.NextDouble() * center.Y * 2;
                var wiggle = new Point(wigX, wigY);
                _position = new Rect(wiggle, _size);
                _brush = new CanvasSolidColorBrush(canvas, Color.FromArgb(100, 225, (byte)(153 + rdm.Next(0, 80)), 255));
                _format = new CanvasTextFormat()
                {
                    FontFamily = "Segoe MDL2 Assets",
                    FontSize = 80 + rdm.Next(80)
                };
                _speed = (maxspeed - minspeed) * (float)rdm.NextDouble() + minspeed;
            }

            public void Draw(CanvasDrawingSession ds, float time, AudioAnalyzer anal)
            {
                _brush.Opacity = Lifetime;
                _position = new Rect(_position.X, _position.Y - _speed, _size.Width, _size.Height);
                ds.DrawText("\uEB51", _position, _brush, _format);
            }
        }

        Random _rdm = new Random();
        CanvasRenderTarget _rt;
        AudioAnalyzer _analyst;
        ICanvasAnimatedControl _canvas;
        CanvasDrawingSession _ds;
        Vector2 _center;
        CanvasBitmap _heartbmp;

        Queue<Heart> _hrtz;
        

        public int MaximumHearts = 13;
        public float BassMaximum = 3f;
        private float _actualBassMax = 4.0f;
        public float LifeDrain = 0.01f;
        public float Minspeed = 0.5f;
        public float Maxspeed = 4f;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _hrtz = new Queue<Heart>(capacity: MaximumHearts);
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _analyst = analyzer;
            _canvas = canvas;
            _ds = _rt.CreateDrawingSession();
            _center = new Vector2((float)_canvas.Size.Width / 2f, (float)_canvas.Size.Height / 2f);
        }

        public Hearts(CanvasBitmap heart)
        {
            _heartbmp = heart;
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            _ds.DrawImage(target);

            if (_hrtz.Count > 0) {
                foreach (var hrt in _hrtz)
                {
                    hrt.Draw(_ds, time, _analyst);
                    hrt.Lifetime -= LifeDrain;
                }

                if (_hrtz.Peek().Lifetime < 0f)
                {
                    _hrtz.Dequeue();
                }
            } else
            {
                _actualBassMax = BassMaximum;
            }

            if (_hrtz.Count < MaximumHearts && _analyst.BassLevel > BassMaximum)
            {
                var heart = new Heart(_canvas, _rdm, _center, Minspeed, Maxspeed);
                _hrtz.Enqueue(heart);
                _actualBassMax += 0.1f;
            } else
            {
                if (_actualBassMax > BassMaximum)
                    _actualBassMax -= 0.0083f;
            }
            
            return _rt;
        }
    }
}
