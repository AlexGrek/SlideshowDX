﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System.Numerics;

namespace SlideshowDX.Effects
{
    class ZoomBass : IEffect
    {
        CanvasRenderTarget _rt;
        AudioAnalyzer _analyst;
        ICanvasAnimatedControl _canvas;
        CanvasDrawingSession _ds;
        Vector2 _center;

        float Divisor = 200f;


        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _analyst = analyzer;
            _canvas = canvas;
            _ds = _rt.CreateDrawingSession();
            _center = new Vector2((float)_canvas.Size.Width / 2f, (float)_canvas.Size.Height / 2f);
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            _ds.Clear(Windows.UI.Colors.Black);
            var sender = _canvas;
            _ds.Transform = Matrix3x2.CreateScale(_analyst.BassLevel / Divisor + 1f,
                _center);
            _ds.DrawImage(target);
            _ds.Transform = new Matrix3x2();
            _ds.Flush();

            return _rt;
        }
    }
}
