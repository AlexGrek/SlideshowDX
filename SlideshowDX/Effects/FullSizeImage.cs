﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace SlideshowDX.Effects
{
    class FullSizeImage
    {
        CanvasRenderTarget _rt;
        CanvasDrawingSession _ds;
        CanvasBitmap _bmp;
        Rect _rectsrc;
        Rect _rectdest;

        public FullSizeImage(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer, CanvasBitmap bmp)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _bmp = bmp;

            var aspect = (float)_bmp.SizeInPixels.Width / _bmp.SizeInPixels.Height;
            var h = canvas.Size.Height * aspect;
            var posw = canvas.Size.Width / 2 - h / 2;

            _rectdest = new Rect(posw, 0, canvas.Size.Height * aspect,
                canvas.Size.Height);
            _rectsrc = new Rect(0, 0,
                 _bmp.Size.Width, _bmp.Size.Height);

            _ds = _rt.CreateDrawingSession();
        }

        public CanvasRenderTarget RenderImage()
        {
            if (_bmp != null)
            {
                var ds = _ds;
                ds.DrawImage
                    (_bmp as ICanvasImage, _rectdest, _rectsrc);
                ds.Flush();
            }
            return _rt;
        }
    }
}

