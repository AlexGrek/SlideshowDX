﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Effects
{
    interface IEffect
    {
        CanvasRenderTarget Render(CanvasRenderTarget target, float time);
        void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer);
    }
}
