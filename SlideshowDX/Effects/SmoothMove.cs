﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Diagnostics;
using System.Numerics;

namespace SlideshowDX.Effects
{
    class SmoothMove : IEffect
    {
        CanvasRenderTarget _rt;
        AudioAnalyzer _analyst;
        ICanvasAnimatedControl _canvas;
        CanvasDrawingSession _ds;
        Vector2 _center;
        float _textPosition;
        CanvasBitmap _bmp;
        float _h, _w;

        float _mzoom = 0.2f;
        float _dzoom;

        // focal points
        Vector2 _focal1;
        Vector2 _focal2;

        Random _rdm = new Random();

        // to handle size
        public SmoothMove(CanvasBitmap bmp, float x, float y)
        {
            _bmp = bmp;
            _w = x; _h = y;
        }

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _analyst = analyzer;
            _canvas = canvas;
            _ds = _rt.CreateDrawingSession();
            _center = new Vector2((float)_canvas.Size.Width / 2f, (float)_canvas.Size.Height / 2f);
            _textPosition = (float)(_rdm.NextDouble() * 400);
            GenerateFocalPoints();
            _dzoom = (float)_rdm.NextDouble() * 0.4f;
        }

        public void GenerateFocalPoints()
        {
            double w, h;
            float shiftx = 0;
            float mzoom = _mzoom;
            if (_w > _h)
            {
                var aspect = _w / _h; // > 1
                w = _canvas.Size.Height * aspect;
                h = _canvas.Size.Height;
                shiftx = (float)(_canvas.Size.Width - w) / 2f;

                var canvaspect = _canvas.Size.Width / _canvas.Size.Height;
                var aspectdiff =  canvaspect - aspect;
                _mzoom += (float)aspectdiff / 2f;
                Debug.WriteLine($"AspectDiff: {canvaspect - aspect}");
            } else
            {
                var aspect = _w / _h; // < 1

                w = _canvas.Size.Width;
                h = _canvas.Size.Height;

                shiftx = (float)(_canvas.Size.Width - w) / 2f;
            }
            
            var z = 1 - (mzoom / 2);

            var lenw = w - w * z;
            var lenh = h - z * h;

            var x = (float)(_rdm.NextDouble() * lenw + z * w / 2) + shiftx;
            var y = (float)(_rdm.NextDouble() * lenh + z * h / 2);
            _focal1 = new Vector2(x, y);

            var x2 = (float)(_rdm.NextDouble() * lenw + z * w / 2) + shiftx;
            var y2 = (float)(_rdm.NextDouble() * lenh + z * h / 2);
            _focal2 = new Vector2(x2, y2);

            Debug.WriteLine($"F1: {_focal1}, F2: {_focal2}, [w: {w} h: {h}] _mzoom = {_mzoom}");
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            var targetPoint = Vector2.Lerp(_focal1, _focal2, time);
            var move = Vector2.Subtract(_center, targetPoint);

            _ds.Clear(Windows.UI.Colors.Black);
            var sender = _canvas;
            _ds.Transform = Matrix3x2.CreateScale(1 + _mzoom + _mzoom * time * _dzoom,
                targetPoint);
            _ds.Transform *= Matrix3x2.CreateTranslation(move);
            _ds.DrawImage(target);

            // Debug: draw focal points
            //_ds.DrawCircle(_focal1, 90f, Windows.UI.Colors.Aqua);
            //_ds.DrawCircle(_focal2, 90f, Windows.UI.Colors.Gold);
            //_ds.DrawCircle(targetPoint, 200f, Windows.UI.Colors.OrangeRed);

            //_ds.DrawText(time.ToString(), 700, _textPosition + 200, Windows.UI.Colors.White);
            
            _ds.Flush();

            return _rt;
        }
    }
}
