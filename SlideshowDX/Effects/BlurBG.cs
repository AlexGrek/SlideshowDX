﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas.Effects;
using Windows.UI;

namespace SlideshowDX.Effects
{
    class BlurBG : IEffect
    {
        CanvasRenderTarget _rt;
        ArithmeticCompositeEffect _fx;
        AudioAnalyzer _analyst;
        CanvasDrawingSession _ds;
        FullSizeImage _bg;
        CanvasBitmap _bgi;
        ICanvasAnimatedControl _sender;
        float _blurlevel;

        public float Cooldown = 0.01f;
        public float Divisor = 5.2f;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _analyst = analyzer;
            _ds = _rt.CreateDrawingSession();
            _sender = canvas;
            _fx = new ArithmeticCompositeEffect()
            {
                Source1Amount = 1,
                Source2Amount = 1,
                MultiplyAmount = 0,
                Offset = 0
            };

            // create additional FullSizeImage for background
            _bg = new FullSizeImage(canvas, analyzer, _bgi);
        }

        public BlurBG(CanvasBitmap bg)
        {
            _bgi = bg;
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            //if (_analyst.BassLevel > _maxBass)
            //    _maxBass = _analyst.BassLevel;

            //var dt = _ds;
            //_effect.Source = target;
            //_effect.BlurAmount = 0; // _analyst.BassLevel > 1 ? _analyst.BassLevel : 0;
            //dt.DrawImage(_effect);
            ////dt.DrawText(time.ToString(), 200, 100, Colors.Yellow);
            ////dt.DrawRectangle((_analyst.BassLevel / _maxBass).ToString(), 100, 250, Colors.Yellow);
            //dt.DrawRectangle(new Windows.Foundation.Rect(100, 250, _analyst.BassLevel / _maxBass * 10, 10), Colors.Cyan);
            ////dt.DrawText(_maxBass.ToString(), 400, 100, Colors.Yellow);
            var blevel = _analyst.BassLevel / Divisor;
            if (blevel > _blurlevel)
            {
                _blurlevel = blevel;
            }
            else
            {
                _blurlevel -= Cooldown;
                if (_blurlevel < 0f)
                    _blurlevel = 0f;
            }

            _fx.Source1 = _bg.RenderImage();
            _fx.Source2 = target;
            _fx.Source1Amount = _blurlevel;
            _fx.Source2Amount = 1 - _blurlevel;

            var destrect = new Windows.Foundation.Rect(0, 0, _sender.Size.Width, _sender.Size.Height);
            var srcrect = new Windows.Foundation.Rect(0, 0, target.Size.Width, target.Size.Height);

            _ds.DrawImage(_fx, destrect, srcrect);
            _ds.Flush();

            return _rt;
        }
    }
}
