﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Effects
{
    public class EffectsSettings
    {
        public bool SmoothMoveEnabled;
        public bool BlurBGEnabled;
        public bool HeartsEnabled;
        public bool ZoomBassEnabled;
        public bool VignetteEnabled;

        // Vignette
        public Windows.UI.Color VignetteColor = Windows.UI.Colors.LightPink;
        public float VignetteSize = 0.3f;
        public float VignetteCooldown = 0.01f;
        public float VignetteDivisor = 15f;

        // ZoomBass
        public float ZoomBassDivisor = 200f;

        // BlurBG
        public float BlurBGCooldown = 0.01f;
        public float BlurBGDivisor = 5.2f;
    }
}
