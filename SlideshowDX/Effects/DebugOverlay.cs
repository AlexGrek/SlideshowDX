﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas.Effects;
using Windows.UI;
using System.Collections.Generic;

namespace SlideshowDX.Effects
{
    class DebugOverlay : IEffect
    {
        CanvasRenderTarget _rt;
        GaussianBlurEffect _effect;
        AudioAnalyzer _analyst;
        CanvasDrawingSession _ds;
        Pipeline _pipeline;
        LinkedList<string> _log = new LinkedList<string>();
        object _mutex = new object();

        public DebugOverlay(Pipeline pipeline)
        {
            _pipeline = pipeline;
        }

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _effect = new GaussianBlurEffect();
            _analyst = analyzer;
            _ds = _rt.CreateDrawingSession();
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            float maxBass;
            _pipeline.DebugMe(out maxBass);

            var dt = _ds;
            _effect.Source = target;
            _effect.BlurAmount = 0; // _analyst.BassLevel > 1 ? _analyst.BassLevel : 0;
            dt.DrawImage(_effect);
            //dt.DrawText(time.ToString(), 200, 100, Colors.Yellow);
            //dt.DrawRectangle((_analyst.BassLevel / _maxBass).ToString(), 100, 250, Colors.Yellow);
            dt.DrawRectangle(new Windows.Foundation.Rect(100, 250, maxBass * 100, 10), Colors.Red);
            dt.FillRectangle(new Windows.Foundation.Rect(100, 250, _analyst.BassLevel / maxBass * 100, 10), Colors.Cyan);
            //dt.DrawText(_maxBass.ToString(), 400, 100, Colors.Yellow);
            DrawLog();
            dt.Flush();

            return _rt;
        }

        public void DrawLog()
        {
            float position = 300f;
            lock (_mutex)
                foreach (var str in _log)
                {
                    _ds.DrawText(str, 40, position, Colors.LightPink);
                    position += 18f;
                }
        }

        public string Log(string text)
        {
            lock (_mutex)
                _log.AddFirst(text);
            return text;
        }
    }
}
