﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System.Numerics;
using Windows.Foundation;

namespace SlideshowDX.Effects
{
    class Vignette : IEffect
    {
        CanvasRenderTarget _rt;
        VignetteEffect _fx;
        AudioAnalyzer _analyst;
        ICanvasAnimatedControl _canvas;
        CanvasDrawingSession _ds;
        Vector2 _center;
        float _level;


        public Windows.UI.Color Color = Windows.UI.Colors.Pink;
        public float Cooldown = 0.01f;
        public float Divisor = 15f;
        public float Size = 0.3f;

        public void Load(ICanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _rt = new CanvasRenderTarget(canvas, canvas.Size);
            _analyst = analyzer;
            _canvas = canvas;
            _ds = _rt.CreateDrawingSession();
            _center = new Vector2((float)_canvas.Size.Width / 2f, (float)_canvas.Size.Height / 2f);
            _fx = new VignetteEffect();
            _fx.Color = Color;
        }

        public CanvasRenderTarget Render(CanvasRenderTarget target, float time)
        {
            var blevel = _analyst.BassLevel / Divisor;
            if (blevel > _level)
            {
                _level = blevel;
            }
            else
            {
                _level -= Cooldown;
                if (_level < 0f)
                    _level = 0f;
            }

            _ds.Clear(Windows.UI.Colors.Black);
            _fx.Source = target;
            _fx.Amount = _level;
            _fx.Curve = Size;
            _ds.DrawImage(_fx);

            return _rt;
        }
    }
}
