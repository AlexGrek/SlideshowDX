﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;
using Microsoft.Graphics.Canvas.UI.Xaml;

namespace SlideshowDX
{
    class ExamplePictureStatic
    {
        CanvasDevice _device;
        CanvasBitmap _bg;
        CanvasBitmap _fg;
        ArithmeticCompositeEffect _fx;
        bool _loaded = false;


        public ExamplePictureStatic(CanvasDevice dev)
        {
            _device = dev;
        }

        public async Task Load()
        {
            StorageFolder InstallationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            string path1 = @"Assets\a.jpg";
            string path2 = @"Assets\a_bg.jpg";

            StorageFile file1 = await InstallationFolder.GetFileAsync(path1);
            StorageFile file2 = await InstallationFolder.GetFileAsync(path2);
                
            _fg = await CanvasBitmap.LoadAsync(_device, file1.Path);
            _bg = await CanvasBitmap.LoadAsync(_device, file2.Path);

            
            _fx = new ArithmeticCompositeEffect()
            {
                Source1 = _bg,
                Source2 = _fg,
                Source1Amount = 1,
                Source2Amount = 1,
                MultiplyAmount = 0,
                Offset = 0
            };

            _loaded = true;
        }

        public void Render(CanvasControl sender, CanvasDrawEventArgs args, AudioAnalyzer analyst)
        {
            var session = args.DrawingSession;
            if (!_loaded)
                return;

            _fx.Source1Amount = analyst.BassLevel / 2;
            _fx.Source2Amount = 1 - analyst.BassLevel / 2;

            var destrect = new Windows.Foundation.Rect(0, 0, sender.Size.Width, sender.Size.Height);
            var srcrect = new Windows.Foundation.Rect(0, 0, _fg.SizeInPixels.Width, _fg.SizeInPixels.Height);

            args.DrawingSession.DrawImage
                (_fx as ICanvasImage, destrect, srcrect);
        }
    }
}
