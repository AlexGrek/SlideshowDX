﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlideshowDX.Effects
{
    class EffectFactory
    {
        const string PATH_TO_HEARTS = "Assets/heart";

        private EffectsSettings _parameters;
        private CanvasAnimatedControl _canvas;
        private AudioAnalyzer _analyst;
        private bool _loaded = false;
        CanvasBitmap _heartsbmp;

        public EffectFactory(EffectsSettings sets, CanvasAnimatedControl canvas, AudioAnalyzer analyzer)
        {
            _parameters = sets;
            _canvas = canvas;
            _analyst = analyzer;
        }

        public void AddEffectsPipeline(IList<IEffect> effects, CanvasBitmap bmp, CanvasBitmap background = null)
        {
            if (!_loaded)
                throw new Exception("EffectFactory.AddEffectsPipeline called before Load() finished");

            // blurred BG
            if (background != null && _parameters.BlurBGEnabled)
            {
                var blur = new BlurBG(background);
                blur.Load(_canvas, _analyst);
                effects.Add(blur);
            }
            if (_parameters.SmoothMoveEnabled)
            {
                var smooth = new SmoothMove(bmp, (float)bmp.Size.Width, (float)bmp.Size.Height);
                smooth.Load(_canvas, _analyst);
                effects.Add(smooth);
            }
            if (_parameters.VignetteEnabled)
            {
                var vignette = new Vignette();
                vignette.Color = _parameters.VignetteColor;
                vignette.Load(_canvas, _analyst);
                effects.Add(vignette);
            }
            if (_parameters.ZoomBassEnabled)
            {
                var basszoom = new ZoomBass();
                basszoom.Load(_canvas, _analyst);
                effects.Add(basszoom);
            }
            if (_parameters.HeartsEnabled)
            {
                var fx = new Hearts(_heartsbmp); //TODO: pass corrrect parameter
                fx.Load(_canvas, _analyst);
                effects.Add(fx);
            }
        }

        public async Task Load()
        {
            // TODO: load hearts bitmap. If needed...
            
            _loaded = true;
        }
    }
}
