﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas;
using Windows.Storage;
using Windows.Foundation;

namespace SlideshowDX.Effects
{
    class JustImage : DrawingRoot
    {
        CanvasBitmap _bmp;
        CanvasDevice _device;
        float Zoom = 0.05f;

        public async Task Load()
        {
            StorageFolder InstallationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            string path1 = @"Assets\background.jpg";

            StorageFile file1 = await InstallationFolder.GetFileAsync(path1);

            _bmp = await CanvasBitmap.LoadAsync(_device, file1.Path);
        }

        public void Render(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args, AudioAnalyzer anal, float time)
        {
            if (_bmp != null)
                using (CanvasDrawingSession ds = args.DrawingSession)
                {
                    var halfW = sender.Size.Width * Zoom / 2;
                    var halfH = sender.Size.Height * Zoom / 2;

                    var destrect = new Rect(0 * Zoom - halfW,
                        0 * Zoom - halfH,
                        sender.Size.Width + sender.Size.Width * Zoom,
                        sender.Size.Height + sender.Size.Height * Zoom);
                    var srcrect = new Rect(0, 0,
                        _bmp.SizeInPixels.Width + _bmp.SizeInPixels.Width * Zoom,
                        _bmp.SizeInPixels.Height + _bmp.SizeInPixels.Height * Zoom);
                    args.DrawingSession.DrawImage
                        (_bmp as ICanvasImage, destrect, srcrect);
                }
        }

        public JustImage(CanvasDevice dev, CanvasBitmap bitmap)
        {
            _bmp = bitmap;
            _device = dev;
        }
    }
}
