﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Windows.UI;

namespace SlideshowDX.Effects
{
    class DebugInfo : DrawingRoot
    {
        public void Render(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args, AudioAnalyzer anal, float time)
        {
            using (var dt = args.DrawingSession)
            {
                dt.DrawText(time.ToString(), 200, 100, Colors.Yellow);
            }
        }
    }
}
