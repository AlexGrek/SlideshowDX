﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace SlideshowDX
{
    class Pipeline
    {
        private StorageFile[] _files;

        // AUDIO ANALYSIS
        float _maxBass = 3;

        CanvasBitmap _bmpCurrent;
        CanvasBitmap _bmpNext;
        AudioAnalyzer _analyzer;
        CanvasAnimatedControl _canvas;
        float _avgDelay;
        float _minDelay;
        float _globalTime = 0; //ms
        float _nextTime = 0; // ms, time of next image, not normalized
        float _currentTime = 0; // ms, time of current image, not normalized
        float _lastResetTime = 0;
        float _avgTransitionTime = 500;
        float _transitionTime = 0;
        bool _isTransition = false;

        Dictionary<string, StorageFile> _blurrs; // filename -> blur StorageFile

        Effects.DebugOverlay _debugOverlay;

        // parameters
        SlideshowParams _parameters;

        Transitions.ITransition _currentTransition;
        Transitions.ITransition _nextTransition; // temporary unused

        Transitions.TransitionFactory _transitionFactory;
        Effects.EffectFactory _effectFactory;

        Guardian _guard;
        bool _isPaused = false;

        List<Effects.IEffect> _currentPipe;
        Effects.FullSizeImage _currentRoot;

        List<Effects.IEffect> _nextPipe;
        Effects.FullSizeImage _nextRoot;


        public void SetNextBitmap(CanvasBitmap bitmap, CanvasBitmap background)
        {
            _bmpNext = bitmap;
            CreateNextEffect(bitmap, background);
            _nextTransition = _transitionFactory.CreateSpecial(_canvas, _analyzer);
            _debugOverlay.Log("-> Loaded next bitmap");
        }

        public void CreateNextEffect(CanvasBitmap bmp, CanvasBitmap background)
        {
            _nextRoot = new Effects.FullSizeImage(_canvas, _analyzer, bmp);
            _nextPipe = new List<Effects.IEffect>();

            // call effect factory
            _effectFactory.AddEffectsPipeline(_nextPipe, bmp, background);
        }

        /// <summary>
        /// Main method
        /// </summary>
        public void Draw(ICanvasAnimatedControl dev, CanvasAnimatedDrawEventArgs args)
        {
            var dt = DeltaTime();

            _globalTime += dt;
            _currentTime += dt;
            var thistime = _globalTime - _lastResetTime;
            var normalizedTime = _currentTime / _avgDelay;

            if (_currentPipe == null)
                return;

            var root = _currentRoot.RenderImage();
            var current = root;
            if (normalizedTime <= 0f)
                Debug.WriteLine($"What the fuck just happened? {normalizedTime}");
            for (int i = 0; i < _currentPipe.Count; i++)
            {
                current = _currentPipe[i].Render(current, normalizedTime);
            }
            if (!_isTransition)
            {
                // regular draw, without any transitions, quick
                args.DrawingSession.Clear(Windows.UI.Colors.Black);
#if DEBUG
                current = _debugOverlay.Render(current, normalizedTime);
# endif
                args.DrawingSession.DrawImage(current);
            } else
            {
                // THIS IS TRANSITION! So...
                _nextTime += dt;
                var normalizedNextTime = _nextTime / _avgDelay;
                // render new image
                var nxtroot = _nextRoot.RenderImage();
                var nxt = nxtroot;
                //if (_nextPipe.Count < 2)
                //    Debug.WriteLine($"What the fuck just happened? {_nextPipe.Count}");
                for (int i = 0; i < _nextPipe.Count; i++)
                {
                    nxt = _nextPipe[i].Render(nxt, normalizedNextTime);
                }

                //and render transition
                var transition = _currentTransition.Render(current, nxt, _transitionTime);
                args.DrawingSession.Clear(Windows.UI.Colors.Blue);
#if DEBUG
                transition = _debugOverlay.Render(transition, normalizedTime); 
#endif
                args.DrawingSession.DrawImage(transition);
            }

            SyncWithMusic(dt);

            if (thistime > _avgDelay - _avgTransitionTime && !_isTransition)
            {
                GoTransition();
                AdaptTimeMore();
            }

            if (_isTransition)
            {
                _transitionTime += dt / _avgTransitionTime;
                if (_transitionTime >= 1)
                    GoNext();
            }
        }

        private void SyncWithMusic(float dt)
        {
            if (_maxBass > 1.5f)
                _maxBass -= dt * 0.0005f;

            if (_analyzer.BassLevel > _maxBass)
            {
                _debugOverlay.Log($"))) {_maxBass} -> {_analyzer.BassLevel}");
                Debug.WriteLine($"))) {_maxBass}");
                _maxBass = _analyzer.BassLevel;
                if (_currentTime > _minDelay && !_isTransition)
                {
                    GoTransition();
                    AdaptTimeLess();
                }
            }
        }

        private void AdaptTimeLess()
        {
            if (!_parameters.Adaptive)
                return;

            if (_avgDelay - _minDelay > 500f)
            {
                var difference = _avgDelay - _currentTime;
                if (difference > 200f)
                    _avgDelay -= difference * 0.2f;
                var mindifference = _currentTime - _minDelay;
                if (mindifference > 200f)
                    _minDelay += mindifference * 0.1f;
            }
            Debug.WriteLine($"Adapted -: {_avgDelay} avg, {_minDelay} min");
        }

        private void AdaptTimeMore()
        {
            if (!_parameters.Adaptive)
                return;

            if (_avgDelay - _minDelay < 3000f)
            {
                _avgDelay += _minDelay * 0.1f;
                if (_minDelay > 100f)
                    _minDelay -= 50f;
            }
            Debug.WriteLine($"Adapted +: {_avgDelay} avg, {_minDelay} min");
        }

        public void GoTransition()
        {
            _isTransition = true;
        }

        public void GoNext()
        {
            //Debug.WriteLine($"GoNext() triggered at {_lastResetTime} :: {_currentTime}");
            _currentRoot = _nextRoot;
            _currentPipe = _nextPipe;
            _lastResetTime = _globalTime;

            // stop transition
            _isTransition = false;
            _transitionTime = 0;

            // use next transition
            _currentTransition = _nextTransition;

            //reset time
            _currentTime = _nextTime;
            _nextTime = 0;

            // require next
            _guard.Required += 1;
        }

        public float DeltaTime()
        {
            if (_isPaused)
                return 0;
            return 16.666f;
        }

        public Pipeline(StorageFile[] files, SlideshowParams par, Dictionary<string, StorageFile> blurrs)
        {
            var copiedf = new StorageFile[files.Length];
            files.CopyTo(copiedf, 0);
            _files = copiedf;
            _avgDelay = (float)par.AvgDelay * 1000f; // second -> ms
            _avgTransitionTime = (float)par.TransitionTime;
            _minDelay = (float)par.MinDelay * 1000f; // second -> ms
            _parameters = par;

            // blurred BG support
            _blurrs = blurrs; 
        }

        public async Task CreateFactories(SlideshowParams par, CanvasAnimatedControl canvas)
        {
            _effectFactory = new Effects.EffectFactory(par.EffectSet, canvas, _analyzer);
            await _effectFactory.Load();
            _transitionFactory = new Transitions.TransitionFactory(par.TransitionSet);
            await _transitionFactory.Load();
        }


        public async Task LoadInitialAsync(CanvasAnimatedControl sender, AudioAnalyzer analyzer)
        {
            _analyzer = analyzer;
            _currentTransition = new Transitions.Fade();
            _currentTransition.Load(sender, _analyzer);
            _canvas = sender;

            StorageFile file1 = _files[0];
            using (IRandomAccessStream fileStream = await file1.OpenReadAsync())
            {
                _bmpCurrent = await CanvasBitmap.LoadAsync(sender.Device, fileStream);
            }

            await CreateFactories(_parameters, sender);

            _guard = new Guardian(_files, _blurrs, this)
            {
                Required = 1
            };
            _guard.RunForever(sender);
            _currentRoot = new Effects.FullSizeImage(sender, _analyzer, _bmpCurrent);

            _debugOverlay = new Effects.DebugOverlay(this);
            _debugOverlay.Load(_canvas, _analyzer);

            _currentPipe = new List<Effects.IEffect>();
        }

        public void DebugMe(out float maxBass)
        {
            maxBass = _maxBass;
        }

        public void SetPause(bool paused)
        {
            _isPaused = paused;
        }
    }
}
