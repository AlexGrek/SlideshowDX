﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Toolkit.Uwp.UI.Animations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.ViewManagement;
using System.Threading.Tasks;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SlideshowDX
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainMenu : Page
    {
        CanvasBitmap _bmp;
        public float Zoom = 0.05f;

        float mouseDiffX, mouseDiffY;


        public MainMenu()
        {
            this.InitializeComponent();
            var view = ApplicationView.GetForCurrentView();
            view.TryEnterFullScreenMode();
        }

        void canvasControl_Draw(CanvasControl sender, CanvasDrawEventArgs args)
        {
            if (_bmp != null)
                using (CanvasDrawingSession ds = args.DrawingSession)
                {
                    var halfW = sender.Size.Width * Zoom / 2;
                    var halfH = sender.Size.Height * Zoom / 2;

                    var destrect = new Rect(-mouseDiffX * Zoom - halfW, 
                        -mouseDiffY * Zoom - halfH, 
                        sender.Size.Width + sender.Size.Width * Zoom,
                        sender.Size.Height + sender.Size.Height * Zoom);
                    var srcrect = new Rect(0, 0,
                        _bmp.SizeInPixels.Width + _bmp.SizeInPixels.Width * Zoom,
                        _bmp.SizeInPixels.Height + _bmp.SizeInPixels.Height * Zoom);
                    args.DrawingSession.DrawImage
                        (_bmp as ICanvasImage, destrect, srcrect);
                }
        }

        private async void myLittleCanvas_CreateResources(CanvasControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            StorageFolder InstallationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            string path1 = @"Assets\background.jpg";

            StorageFile file1 = await InstallationFolder.GetFileAsync(path1);

            _bmp = await CanvasBitmap.LoadAsync(sender.Device, file1.Path);
            myLittleCanvas.Invalidate();
        }

        private void myLittleCanvas_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var point = e.GetCurrentPoint(FakeCenter);
            mouseDiffX = (float)point.Position.X;
            mouseDiffY = (float)point.Position.Y;
            myLittleCanvas.Invalidate();
        }

        private async void AudioPageOpen(object sender, RoutedEventArgs e)
        {
            await TotalGrid.Blur(value: 30f, duration: 1000).Fade(value: 0f, duration: 1000).StartAsync();
            Frame.Navigate(typeof(MainPage));
        }

        private async void DemoPageOpen(object sender, RoutedEventArgs e)
        {
            string filename = @"Assets\Audio\audio.mp3";
            string exname = @"Assets\Examples";
            StorageFile music = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(filename);
            StorageFolder folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(exname);
            var files = await folder.GetFilesAsync();
            // Demo
            var esettings = new Effects.EffectsSettings()
            {
                ZoomBassEnabled = true,
                VignetteEnabled = true,
                BlurBGEnabled = true,
                SmoothMoveEnabled = true
            };
            var tsettings = new Transitions.TransitionSettings()
            {
                FadeEnabled = true,
                FlipEnabled = true,
                FadeShiftEnabled = true
            };
            var parameters = new SlideshowParams
            {
                Audio = music,
                Slides = files.ToArray(),
                AvgDelay = 3.0,
                MinDelay = 1.0,
                TransitionTime = 400,
                Adaptive = true,
                TransitionSet = tsettings,
                EffectSet = esettings
            };
            Frame.Navigate(typeof(SlideshowPage), parameters);
        }

        private async void NavigateToPics(object sender, RoutedEventArgs e)
        {
            await TotalGrid.Blur(value: 50f, duration: 600).Fade(value: 0f, duration: 600).StartAsync();
            this.Frame.Navigate(typeof(SelectDirectoryPage));
        }
    }
}
