﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MathNet.Numerics;
using MathNet.Numerics.IntegralTransforms;

namespace SlideshowDX
{
    class AudioAnalyzer
    {
        bool _valid = false;
        object _mutex = new object();

        int _size = 0;
        float _totatMagnitude = 0;
        float _bass = 0;
        float _previousBass = 0;
        float[] _real;
        float[] _waveform1;
        float[] _waveform2;
        float[] _imaginary;
        float[] _magnitude;
        float[] _core;

        public AudioAnalyzer(int sizeOfVectors)
        {
            _size = sizeOfVectors;
            _totatMagnitude = 0f;
            _real = new float[sizeOfVectors];
            _waveform1 = new float[sizeOfVectors];
            _waveform2 = new float[sizeOfVectors];
            _imaginary = new float[sizeOfVectors];
            _magnitude = new float[sizeOfVectors];
            _core = new float[sizeOfVectors];
        }

        public unsafe void NextFrame(float* dataInFloat)
        {
            // synchronize write operation
            lock (_mutex)
            {
                for (int i = 1; i < _size; i += 1)
                {
                    // invalidate everything
                    _core[i] = dataInFloat[i];
                }
            }
            _valid = false;
        }

        public void Calculate()
        {
            lock (_mutex)
            {
                // copy data to C# array from vector
                // and fill imaginary[] with 0
                for (int i = 1; i < _size; i += 1)
                {
                    _real[i] = _core[i];
                    _waveform1[i] = _core[i];
                    _waveform2[i] = _core[i];
                    _imaginary[i] = 0f;
                }
            }

            // get FFT from current frame
            Fourier.Forward(_real, _imaginary);

            for (int i = 1; i < _size / 2; i += 1)
            {
                _magnitude[i] = (float)Math.Sqrt(_real[i] * _real[i] + _imaginary[i] * _imaginary[i]);
            }

            // bass level calculation
            var newbass = _magnitude[0] * 3 + _magnitude[1] * 2 + _magnitude[2];
            newbass /= 6;
            var xbass = Math.Max(_previousBass, newbass);
            _previousBass = newbass;
            _bass = (_bass + xbass) / 2;

            // mark this frame as valid
            _valid = true;
        }

        public void Validate()
        {
            if (!_valid)
                Calculate();
        }

        public float TotatMagnitude
        {
            get
            {
                Validate();
                return _totatMagnitude;
            }
        }
        public float BassLevel
        {
            get
            {
                Validate();
                return _bass;
            }
        }
        public float PreviousBass
        {
            get
            {
                Validate();
                return _previousBass;
            }
            set
            {
                _previousBass = value;
            }
        }

        public float[] Real
        {
            get
            {
                Validate();
                return _real;
            }
            set
            {
                _real = value;
            }
        }

        public float[] Waveform1
        {
            get
            {
                Validate();
                return _waveform1;
            }
            set
            {
                _waveform1 = value;
            }
        }

        public float[] Waveform2
        {
            get
            {
                Validate();
                return _waveform2;
            }
            set
            {
                _waveform2 = value;
            }
        }

        public float[] Imaginary
        {
            get
            {
                Validate();
                return _imaginary;
            }
            set
            {
                _imaginary = value;
            }
        }

        public float[] Magnitude
        {
            get
            {
                Validate();
                return _magnitude;
            }
            set
            {
                _magnitude = value;
            }
        }
    }
}
