﻿using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Media;
using Windows.Media.Audio;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Effects;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SlideshowDX
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        AudioAnalyzer _analyst = new AudioAnalyzer(512);

        ExamplePictureStatic _fx;

        float _wtf = 1488.0f;
        int _wat = 1212;
        AudioGraph audioGraph;
        AudioFrameOutputNode frameOutputNode;

        private AudioFileInputNode fileInput;
        private AudioDeviceOutputNode deviceOutput;

        [ComImport]
        [Guid("5B0D3235-4DBA-4D44-865E-8F1D0E4FD04D")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        unsafe interface IMemoryBufferByteAccess
        {
            void GetBuffer(out byte* buffer, out uint capacity);
        }

        private void CreateFrameOutputNode()
        {
            frameOutputNode = audioGraph.CreateFrameOutputNode();
            audioGraph.QuantumStarted += AudioGraph_QuantumStarted;
        }

        private void AudioGraph_QuantumStarted(AudioGraph sender, object args)
        {
            AudioFrame frame = frameOutputNode.GetFrame();
            ProcessFrameOutput(frame);
        }

        unsafe private void ProcessFrameOutput(AudioFrame frame)
        {
            using (AudioBuffer buffer = frame.LockBuffer(AudioBufferAccessMode.Write))
            using (IMemoryBufferReference reference = buffer.CreateReference())
            {
                byte* dataInBytes;
                uint capacityInBytes;
                float* dataInFloat;

                // Get the buffer from the AudioFrame
                ((IMemoryBufferByteAccess)reference).GetBuffer(out dataInBytes, out capacityInBytes);

                dataInFloat = (float*)dataInBytes;

                _wtf = 0f;
                _wat = (int)capacityInBytes / sizeof(float);

                // pass to analyst and forget
                _analyst.NextFrame(dataInFloat);

                _wtf = dataInFloat[0];
                myLittleCanvas.Invalidate();
            }
        }

        private async Task InitAudioGraph()
        {

            AudioGraphSettings settings = new AudioGraphSettings(Windows.Media.Render.AudioRenderCategory.Media);

            CreateAudioGraphResult result = await AudioGraph.CreateAsync(settings);
            if (result.Status != AudioGraphCreationStatus.Success)
            {
                Debug.WriteLine("AudioGraph creation error: " + result.Status.ToString());
            }

            audioGraph = result.Graph;

            // INIT DEVICE OUTPUT

            CreateAudioDeviceOutputNodeResult deviceOutputNodeResult = await audioGraph.CreateDeviceOutputNodeAsync();

            if (deviceOutputNodeResult.Status != AudioDeviceNodeCreationStatus.Success)
            {
                // Cannot create device output node
                Debug.WriteLine(String.Format("Device Output unavailable because {0}", deviceOutputNodeResult.Status.ToString()));
                return;
            }

            deviceOutput = deviceOutputNodeResult.DeviceOutputNode;

            _wtf = (float) audioGraph.SamplesPerQuantum;
            myLittleCanvas.Invalidate();

            // INIT FRAME OUTPUT
            CreateFrameOutputNode();
        }


        public async void FuckingMagic()
        {
            await InitAudioGraph();
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.MusicLibrary;
            filePicker.FileTypeFilter.Add(".mp3");
            filePicker.FileTypeFilter.Add(".wav");
            filePicker.FileTypeFilter.Add(".wma");
            filePicker.FileTypeFilter.Add(".m4a");
            filePicker.ViewMode = PickerViewMode.Thumbnail;
            StorageFile file = await filePicker.PickSingleFileAsync();

            // File can be null if cancel is hit in the file picker
            if (file == null)
            {
                return;
            }

            CreateAudioFileInputNodeResult fileInputResult = await audioGraph.CreateFileInputNodeAsync(file);
            if (AudioFileNodeCreationStatus.Success != fileInputResult.Status)
            {
                // Cannot read input file
                Debug.WriteLine(String.Format("Cannot read input file because {0}", fileInputResult.Status.ToString()));
                return;
            }

            fileInput = fileInputResult.FileInputNode;

            if (fileInput.Duration <= TimeSpan.FromSeconds(3))
            {
                // Imported file is too short
                Debug.WriteLine("Please pick an audio file which is longer than 3 seconds");
                fileInput.Dispose();
                fileInput = null;
                return;
            }

            fileInput.AddOutgoingConnection(deviceOutput);
            fileInput.AddOutgoingConnection(frameOutputNode);

            // Trim the file: set the start time to 3 seconds from the beginning
            // fileInput.EndTime can be used to trim from the end of file
            fileInput.StartTime = TimeSpan.FromSeconds(3);

            audioGraph.Start();

            _wtf = (float)audioGraph.SamplesPerQuantum;
            myLittleCanvas.Invalidate();
        }

        public MainPage()
        {
            InitializeComponent();
        }

        void canvasControl_Draw(CanvasControl sender, CanvasDrawEventArgs args)
        {

            _fx.Render(sender, args, _analyst);

            if (_analyst != null)
            {
                for (int i = 0; i < _analyst.Imaginary.Length; i++)
                {
                    float grabbed = _analyst.Imaginary[i];
                    if (grabbed <= 0)
                        args.DrawingSession.DrawRectangle(new Rect(300, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Chocolate);
                    else
                        args.DrawingSession.DrawRectangle(new Rect(300 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.AliceBlue);
                }
                for (int i = 0; i < _analyst.Real.Length; i++)
                {
                    float grabbed = _analyst.Real[i];
                    if (grabbed <= 0)
                        args.DrawingSession.DrawRectangle(new Rect(600, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Snow);
                    else
                        args.DrawingSession.DrawRectangle(new Rect(600 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.LightPink);
                }
                for (int i = 0; i < _analyst.Magnitude.Length; i++)
                {
                    float grabbed = _analyst.Magnitude[i];
                    if (grabbed <= 0)
                        args.DrawingSession.DrawRectangle(new Rect(300 + (i << 3), 800, 8, (-grabbed) * 100), Colors.Aqua);
                    else
                        args.DrawingSession.DrawRectangle(new Rect(300 + (i << 3), 800 - (grabbed) * 100, 8, (grabbed) * 100), Colors.Khaki);
                }

                // WAVEFORMS
                for (int i = 0; i < _analyst.Waveform1.Length; i++)
                {
                    float grabbed = _analyst.Waveform1[i];
                    if (grabbed <= 0)
                        args.DrawingSession.DrawRectangle(new Rect(1200, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Honeydew);
                    else
                        args.DrawingSession.DrawRectangle(new Rect(1200 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.Ivory);
                }

                for (int i = 0; i < _analyst.Waveform2.Length; i++)
                {
                    float grabbed = _analyst.Waveform2[i];
                    if (grabbed <= 0)
                        args.DrawingSession.DrawRectangle(new Rect(1500, 300 + (i << 1), (-grabbed) * 100, 2), Colors.Honeydew);
                    else
                        args.DrawingSession.DrawRectangle(new Rect(1500 - (grabbed) * 100, 300 + (i << 1), (grabbed) * 100, 2), Colors.Ivory);
                }

                

                var rt = new CanvasRenderTarget(sender, sender.Size);
                using (var dt = rt.CreateDrawingSession())
                {
                    dt.DrawEllipse(155, 115, 80, 30, Colors.Black, 3);
                    // dt.DrawText((newbass).ToString(), 100, 100, Colors.Bisque);
                    dt.DrawText(_analyst.BassLevel.ToString(), 200, 100, Colors.Yellow);
                    dt.DrawEllipse(155, 115, 80 + _analyst.BassLevel, 30 + _analyst.BassLevel, Colors.IndianRed, 3);
                }

                var effect = new GaussianBlurEffect()
                {
                    Source = rt,
                    BlurAmount = _analyst.BassLevel
                };

                args.DrawingSession.DrawImage(effect);

                
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FuckingMagic();
        }

        private async void myLittleCanvas_CreateResources(CanvasControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            _fx = new ExamplePictureStatic(sender.Device);
            await _fx.Load();
        }
    }
}
