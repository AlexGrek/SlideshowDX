﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace SlideshowDX
{
    public struct SlideshowParams
    {
        public StorageFile Audio;
        public StorageFile[] Slides;
        public double AvgDelay;
        public double MinDelay;
        public bool Adaptive;
        public double TransitionTime;
        public Effects.EffectsSettings EffectSet;
        public Transitions.TransitionSettings TransitionSet;
    }
}
