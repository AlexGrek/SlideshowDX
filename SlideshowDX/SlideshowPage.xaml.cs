﻿using Microsoft.Graphics.Canvas.UI;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Toolkit.Uwp.UI.Animations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SlideshowDX
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SlideshowPage : Page, IDisposable
    {
        Pipeline _pipeline;
        AudioPlayer _player;
        AudioAnalyzer _analyst;
        bool _loaded = false;

        public SlideshowPage()
        {
            this.InitializeComponent();
        }

        private async void myLittleCanvas_CreateResources(CanvasAnimatedControl sender, CanvasCreateResourcesEventArgs args)
        {
            _analyst = await _player.InitializeAsync();
            await _pipeline.LoadInitialAsync(sender, _analyst);
            _player.Play();

            // delay
            await TotalGrid.Blur(value: 30f, duration: 500).Fade(value: 0f, duration: 500).StartAsync();
            _loaded = true;
            // fade-in
            await TotalGrid.Blur(value: 0f, duration: 500).Fade(value: 1f, duration: 500).StartAsync();
        }

        private void canvasControl_Draw(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args)
        {
            if (!_loaded)
                return;

            _pipeline.Draw(sender, args);
        }

        /// <summary>
        /// For background blur only, filter file array
        /// </summary>
        /// <param name="files">array to filter</param>
        /// <returns>filename -> blurred StorageFIle</returns>
        public Dictionary<string, StorageFile> Filter(ref StorageFile[] files) 
        {
            var dict = new Dictionary<string, StorageFile>();
            var detected = new Stack<StorageFile>();

            for (int i = 0; i < files.Length; i++)
            {
                var file = files[i];
                if (file.Name.StartsWith("_"))
                {
                    var nextName = file.Name.Substring(1);
                    // check if there is a corresponding file with same name but with no '_'
                    if (files.Any(f => f.Name.Equals(nextName)))
                    {
                        detected.Push(file); // to remove it later
                        dict.Add(nextName, file);
                        Debug.WriteLine($"File {nextName} with blurren BG added.");
                    }
                }
            }

            if (detected.Count > 0) // do nothing if nothing detected
            {
                // remove blurrs from pics array
                files = files.Except(detected).ToArray();
            }

            return dict;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var parameters = (SlideshowParams)e.Parameter;

            // let's remove garbage right now!
            GC.Collect();
            GC.WaitForPendingFinalizers();
            // honestly, it was useless

            var slides = parameters.Slides;
            Dictionary<string, StorageFile> blurrs = null;
            if (true) // TODO: add real condition
            {
                blurrs = Filter(ref slides);
            }
            _pipeline = new Pipeline(slides, parameters, blurrs);
            _player = new AudioPlayer(parameters.Audio);
        }

        private void myLittleCanvas_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            var pos = point.Position;

            ControlsBorder.HorizontalAlignment = HorizontalAlignment.Left;
            ControlsBorder.VerticalAlignment = VerticalAlignment.Top;
            ControlsEllipse.HorizontalAlignment = HorizontalAlignment.Left;
            ControlsEllipse.VerticalAlignment = VerticalAlignment.Top;

            var marginc = new Thickness(pos.X - ControlsBorder.Width / 2, pos.Y - ControlsBorder.Height / 2, 0, 0);
            var margine = new Thickness(pos.X - ControlsEllipse.Width / 2, pos.Y - ControlsEllipse.Height / 2, 0, 0);
            ControlsEllipse.Margin = margine;
            ControlsBorder.Margin = marginc;

            AppearControls();
        }

        private void AppearControls()
        {
            ControlsBorder.Visibility = Visibility.Visible;
            ControlsEllipse.Visibility = Visibility.Visible;
            popupAppearance.Begin();
            controlsAppearance.Begin();
            _pipeline.SetPause(true);
        }

        private void GoBack()
        {
            Dispose();
            this.Frame.Navigate(typeof(SelectDirectoryPage));
        }

        private void Unpause()
        {
            _pipeline.SetPause(false);
            ControlsBorder.Visibility = Visibility.Collapsed;
            ControlsEllipse.Visibility = Visibility.Collapsed;
        }

        public void Dispose()
        {
            _loaded = false;
            _pipeline = null;
            _player.Dispose();
        }

        private void UnpauseButtonClick(object sender, RoutedEventArgs e)
        {
            Unpause();
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            GoBack();
        }
    }
}
